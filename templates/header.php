	<title><?php echo $PAGE_TITLE.' - '.$title; ?></title>
	<link href="assets/css/all.min.css" rel="stylesheet">
	<script type="text/javascript" src="assets/js/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="assets/js/popper.min.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="assets/js/bootstrap.min.js"></script>
	<link href="assets/css/all.min.css" rel="stylesheet">
	<link rel="apple-touch-icon" sizes="57x57" href="assets/favicon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="assets/favicon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="assets/favicon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="assets/favicon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="assets/favicon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="assets/favicon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="assets/favicon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="assets/favicon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="assets/favicon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="assets/favicon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="assets/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/favicon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="assets/favicon/favicon-16x16.png">
	<link rel="manifest" href="assets/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<meta name="description" content="Dieses Archiv beinhaltet eine Liste mit über 500 Dokumentationen mit allen wichtigen Informationen auf einem Blick. Und das beste, du kannst die Dokus präzise anhand verschiedener Parameter ganz einfach durchsuchen!">
	<meta name="keywords" content="Dokus,Archiv,Doku Archiv,Doku Liste,Liste,Rodirik">
	<?php 
		if ($darkmode) {
			echo '<link rel="stylesheet" type="text/css" href="assets/css/bootstrap_dark.min.css">';
		} else {
			echo '<link rel="stylesheet" type="text/css" href="assets/css/styles.css">';
		}
	?>
	<style type="text/css">
		.center {
			position: absolute;
			top: 50%;
			left: 50%;
			transform: translate(-50%, -50%);
		}
	</style>
	<link rel="stylesheet" type="text/css" href="assets/css/cookieconsent.min.css" />
	<script src="assets/js/cookieconsent.min.js"></script>
	<script>
	window.addEventListener("load", function(){
	window.cookieconsent.initialise({
	  "palette": {
	    "popup": {
	      "background": "#000"
	    },
	    "button": {
	      "background": "#f1d600"
	    },
	    "cookie": {
	    	"name": "<?php echo $cookie_prefix;?>cookieconsent"
	    }
	  },
	  "theme": "classic",
	  "content": {
	    "dismiss": "Ok!",
	  }
	})});
	</script>