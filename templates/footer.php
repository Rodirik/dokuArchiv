<footer class="page-footer font-small blue" id="footer">

  <div class="footer-copyright text-center py-3">
  	&copy; <?php echo date('Y'); ?> Rodirik.de | <a href="./data.php">Datenschutz</a> | <a href="./changelog.php">Changelog</a> | Hosted at  <a href="https://uberspace.de/" target="_blank" rel="noopener" >Uberspace</a>
  </div>

</footer>

<script>
	$(document).ready(function(){
		var hash = window.location.hash;
		$( hash ).addClass('table-primary');



		$('.delete').on('click', function(){
			var dokuName = $(this).data('name');
			var dokuId = $(this).data('id');
			    $post = $(this);

			// console.log(dokuId);

			$('#deleteModalDokuName').text(dokuName);
			$("#deleteDokuId").val(dokuId);
			$("#deleteModal").modal("show");
		});



		$('.like').on('click', function(){
			var dokuid = $(this).data('id');
			    $post = $(this);

			$.ajax({
				url: 'jsActions.php',
				type: 'post',
				data: {
					'jsaction' : 'like',
					'dokuid': dokuid,
					'action' : '+'
				},
				success: function(response){
					// console.log(response);
					// var like_row = post.parent().find('span.likes_count').text("1337");
					$post.addClass('unlike');
					$post.removeClass('like');
					$post.data('action', 'unlike')
					$post.css('color', 'darkcyan');
					var likeCount = $('#likes_count_' + dokuid).text();
					var likeCount = parseInt(likeCount);
					var likeCount = likeCount + 1;
					$('#likes_count_' + dokuid).text(likeCount);

				}
			});
		});

		// when the user clicks on unlike
		$('.unlike').on('click', function(){
			var dokuid = $(this).data('id');
		    $post = $(this);
		    // console.log(dokuid);

			$.ajax({
				url: 'jsActions.php',
				type: 'post',
				data: {
					'jsaction': 'like',
					'dokuid': dokuid,
					'action' : '-'
				},
				success: function(response){
					console.log(response);
					// $post.find('span.likes_count').text(response + " likes");
					if (response == 'error') {
						var content = 'Test';
   						$('.modal-body').content,function(){
        					$('#errorModal').modal({show:true});
        					return;
    					};
					}
					$post.addClass('like');
					$post.removeClass('unlike');
					$post.css('color', '');
					var likeCount = $('#likes_count_' + dokuid).text();
					var likeCount = parseInt(likeCount);
					var likeCount = likeCount - 1;
					$('#likes_count_' + dokuid).text(likeCount);
				}
			});
		});

		$('.marked').on('click', function(){
			var dokuid = $(this).data('id');
			    $post = $(this);

			$.ajax({
				url: 'jsActions.php',
				type: 'post',
				data: {
					'jsaction' : 'mark',
					'dokuid': dokuid,
					'action' : '+'
				},
				success: function(response){
					// console.log(response);
					// var like_row = post.parent().find('span.likes_count').text("1337");
					$post.addClass('unmark');
					$post.removeClass('marked');
					$post.addClass('fas');
					$post.removeClass('far');

				}
			});
		});

		// when the user clicks on unlike
		$('.unmark').on('click', function(){
			var dokuid = $(this).data('id');
		    $post = $(this);
		    // console.log(dokuid);

			$.ajax({
				url: 'jsActions.php',
				type: 'post',
				data: {
					'jsaction': 'mark',
					'dokuid': dokuid,
					'action' : '-'
				},
				success: function(response){
					// console.log(response);
					// $post.find('span.likes_count').text(response + " likes");
					if (response == 'error') {
						var content = 'Test';
   						$('.modal-body').content,function(){
        					$('#errorModal').modal({show:true});
        					return;
    					};
					}
					$post.addClass('marked');
					$post.removeClass('unmark');
					$post.addClass('far');
					$post.removeClass('fas');
				}
			});
		});

		$('.seen').on('click', function(){
			var dokuid = $(this).data('id');
			    $post = $(this);

			$.ajax({
				url: 'jsActions.php',
				type: 'post',
				data: {
					'jsaction' : 'seen',
					'dokuid': dokuid,
					'action' : '+'
				},
				success: function(response){
					// console.log(response);
					// var like_row = post.parent().find('span.likes_count').text("1337");
					$post.addClass('unseen');
					$post.removeClass('seen');
					$post.addClass('fa-eye-slash');
					$post.removeClass('fa-eye');

				}
			});
		});

		// when the user clicks on unlike
		$('.unseen').on('click', function(){
			var dokuid = $(this).data('id');
		    $post = $(this);
		    // console.log(dokuid);

			$.ajax({
				url: 'jsActions.php',
				type: 'post',
				data: {
					'jsaction': 'seen',
					'dokuid': dokuid,
					'action' : '-'
				},
				success: function(response){
					// console.log(response);
					// $post.find('span.likes_count').text(response + " likes");
					if (response == 'error') {
						var content = 'Test';
   						$('.modal-body').content,function(){
        					$('#errorModal').modal({show:true});
        					return;
    					};
					}
					$post.addClass('seen');
					$post.removeClass('unseen');
					$post.addClass('fa-eye');
					$post.removeClass('fa-eye-slash');
				}
			});
		});
		
	});

	$(document).ready(function(){
		$('[data-toggle="popover"]').popover();
	});

	$(document).ready(function(){
		$('[data-toggle="tooltip"]').tooltip();
	});

	$(document).ready(function() {

    var docHeight = $(window).height();
    var footerHeight = $('#footer').height();
    var footerTop = $('#footer').position().top + footerHeight;

    if (footerTop < docHeight)
        $('#footer').css('margin-top', 10+ (docHeight - footerTop) + 'px');
});

// 	$('#deleteModal').on('show.bs.modal', function (event) {
// 		var button = $(event.relatedTarget);
// 		var dokuName = button.data('dokuName');
// 		// ( "#deleteModalDokuName" ).text(dokuName);
// 		$('#deleteModalDokuName').text(likeCount);
// 		var modal = $(this);
// 		modal.find('#deleteModal').val(dokuName);
// })
</script>