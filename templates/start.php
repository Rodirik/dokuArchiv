<?php

	$requiredFiles = array(

		'inc/settings.php',
		'inc/things.class.php',
		'inc/login.class.php',
		'inc/Paginator.php',
		'vendor/autoload.php'

	);

	foreach ($requiredFiles as $requiredFile) {

		if (file_exists($requiredFile)) {
			require $requiredFile;
		} else {

			echo "Fehler! $requiredFile konnte nicht geladen werden :/";
			exit();

		}
		
	}

	//Die Session starten
	ini_set('session.gc_maxlifetime', 86400);
	session_set_cookie_params(86400);
	session_name($cookie_prefix.'SESSIONID');
	session_start();

	if (isset($_GET['darkmode']) && $_GET['darkmode'] == "deactivate") {
		$darkmode = false;
	} elseif (isset($_COOKIE[$cookie_prefix.'darkmode']) || isset($_GET['darkmode']) && $_GET['darkmode'] == "activate") { 
		$darkmode = true;
	} else {
		$darkmode = false;
	}

	if ($wartung) {
		require 'templates/wartung.php';
		exit();
	}

	$site = basename($_SERVER['PHP_SELF'], '.php') . '.php';

	# showNavigation = Soll die Nav leiste angezeigt werden
	$showNavigation = true;

	# Legt fest das man für die seite eingeloggt sein muss
	$needLogin = false;

	// Legt fest ob eine spezielle berechtigung benötigt wird

	$needPermission = '';

	// $show_header = true;

	# Switch legt für jede seite Namen & parametar wie needLogin fest
	switch (strtolower($site)) {
		case "login.php":
			$CURRENT_PAGE = "login"; 
			$PAGE_TITLE = "Einloggen";
			break;
		case "register.php":
			$CURRENT_PAGE = "register"; 
			$PAGE_TITLE = "Registrieren";
			break;
		case "admincp.php":
			$CURRENT_PAGE = "admincp"; 
			$PAGE_TITLE = "Admin CP";
			$needLogin = true;
			$needPermission = array('admin');
			break;
		case "adddoku.php":
			$CURRENT_PAGE = "adddoku"; 
			$PAGE_TITLE = "Doku hinzufügen";
			break;
		case "usercp.php":
			$CURRENT_PAGE = "usercp"; 
			$PAGE_TITLE = "Mein konto";
			$needLogin = true;
			break;
		case "edit.php":
			$CURRENT_PAGE = "editpage"; 
			$PAGE_TITLE = "Doku editieren";
			break;
		case "modcp.php":
			$CURRENT_PAGE = "modcp"; 
			$PAGE_TITLE = "Mod CP";
			$needLogin = true;
			$needPermission = array('mod', 'admin');
			break;
		case "data.php":
			$CURRENT_PAGE = "dataprivacy"; 
			$PAGE_TITLE = "Datenschutz";
			break;
		default:
			$CURRENT_PAGE = "start";
			$PAGE_TITLE = "Start";
	}

	# Datenbank verbindung aufbauen

		$pdo = new PDO("mysql:host=$mysql_host;dbname=$mysql_db", $mysql_user, $mysql_password);
		$pdo->exec('SET NAMES "utf8mb4";');




		// Login objekt erstellen
		$login = new Login($cookie_prefix);

		// Überprüfung ob ein csrf token gesetzt ist wenn nicht wird ein neuer gesetzt
		if (!isset($_SESSION['csrf_token'])) {
			$_SESSION['csrf_token'] = uniqid('', true);
		}

		
		// Überprüfung ob der user sich ausloggen möchte
		if (isset($_POST['logoff']) && $_POST['token'] == $_SESSION['csrf_token'] ) {
			$login->logout();
			header('Location: ?action=logout');
			exit();
		}

		$logged_in = $login->logged_in();


	 // Überpüfung ob die seite eine anmeldung vorraussetzt
	if ($needLogin && !$logged_in) {
		header('Location: ./index.php?error=nologin');
	}

	if ($logged_in) {

		$getUserInfos = $pdo->prepare("SELECT rolle, status, ban_reason FROM user WHERE userid = ?");
		$getUserInfos->execute(array($_SESSION['id']));
		$userInfos = $getUserInfos->fetch();

		if ($userInfos['status'] == "banned" && !$_SESSION['banned']) {

			$login->logout();

			$_SESSION['banned'] = true;
			$_SESSION['banreason'] = $userInfos['ban_reason'];
			$_SESSION['ban_showed'] = false;

			header('Location: ./index.php');
			exit();
		}

		$_SESSION['rolle'] = $userInfos['rolle'];


	}

	// var_dump($_GET);
	if (isset($_GET['darkmode']) && $_GET['token'] == $_SESSION['csrf_token'] ) {
		if ($_GET['darkmode'] == 'activate') {
			setcookie($cookie_prefix."darkmode", true,  time()+60*60*24*30, '/');
		} else {
			setcookie($cookie_prefix."darkmode", false,  time()+60*60*24*30, '/');
		}
	}

	if (!empty($needPermission)) {
		
		if (!in_array($_SESSION['id'], $needPermission)) {
			
			header('Location: ./index.php?error=nopermission');

		}

	}

	// var_dump($darkmode);


	if (isset($_POST['message'])) {

		if ($_POST['token'] == $_SESSION['csrf_token']) {

			$msg = urldecode($_POST['message']);
	
			$msg = wordwrap($msg, 70, "\r\n");
			$msg .= "\r\n\r\n Email: ".urldecode($_POST['contact'])."\r\n\r\nDiscord Username: ".urldecode($_POST['username']);
			
			mail('ich@rodirik.de', 'Neues feedback zum Dokuarchiv', $msg, 'From: dokuarchiv@rodirik.de');

			$feedback = true;
			
		} else {

		$feedback = "session";

		}

	}

?>