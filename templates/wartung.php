<?php

?>
<!DOCTYPE html>
<html>
<head>
	<?php include "templates/header.php";?>
	<style type="text/css">
		html, body{
			height: 100%;
		}
	</style>
</head>
<body>

<div class="container h-100">
	<div class="row align-items-center h-100">
		<div class="col-12 mx-auto">
			  <table class="mx-auto">
			<tr>
				<td>
					<img src="https://static-cdn.jtvnw.net/emoticons/v1/1276152/3.0">
				</td>
				<td style="width: 10px;"></td>
				<td class="ml-3">
					<h2>Wartungsarbeiten!</h2>
					<h3>Das Dokuarchiv ist gleich wieder verfügbar...</h3>
				</td>
			</tr>
		</table>  
		</div>
	</div>
</div>

</body>
</html>