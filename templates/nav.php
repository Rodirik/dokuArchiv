	<div class="modal fade " id="LoginModal">
		<div class="modal-dialog">
			<div class="modal-content">

				<div class="modal-header">
					<h4 class="modal-title">Login</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>    

				<div class="modal-body">
					<div>
						<a style="width: 100%; color: white; background-color: #7289da;" href="./login.php?provider=discord" class="btn"><i class="fab fa-discord"></i> Login mit Discord</a>
					</div>
					<div class="px-auto mt-2">
						<a style="width: 100%; color: white; background-color: #6441a5;" href="./login.php?provider=twitch" class="btn"><i class="fab fa-twitch"></i> Login mit Twitch</a>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Schließen</button>
				</div>

			</div>
		</div>
	</div> 
	<div class="modal fade" id="FeedbackModal">
		<div class="modal-dialog">
			<div class="modal-content">

				<div class="modal-header">
					<h4 class="modal-title">Feedback</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>    

				<div class="modal-body">
					<form id="feedbackForm" method="post">

						<div class="form-group">

							<label for="feedback">E-mail für eventuelle Nachfragen:</label>
							<input type="email" name="contact" class="form-control">

						</div>
						
						<p>Alternativ:</p>

						<div class="form-group">

							<label for="feedback">Discord Username:</label>
							<input type="text" name="username" class="form-control">

						</div>

						<div class="form-group">

							<label for="feedback">Nachricht:</label>
							<textarea required="" class="form-control" rows="5" id="message" name="message" placeholder="Irgendwas das mir helfen könnte das Dokuarchiv zu verbessern..."></textarea>

						</div>

						<input type="hidden" name="token" value="<?php echo $_SESSION['csrf_token'] ?>">

					</form>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Nee doch nicht...</button>
					<button class="btn btn-primary" type="submit" form="feedbackForm">Senden</button>
				</div>

			</div>
		</div>
	</div> 
	<div class="modal fade " id="errorModal">
		<div class="modal-dialog">
			<div class="modal-content">

				<div class="modal-header">
					<h4 class="modal-title">Fehler</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>    

				<div class="modal-body">
					
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Ok</button>
				</div>

			</div>
		</div>
	</div>

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
	<div class="modal-content">
		<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Doku löschen</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Möchtest du die Doku <b>"<span id="deleteModalDokuName"></span>"</b> wirklich löschen?</p>
			</div>
			<div class="modal-footer">
				
				<form method="get" action="edit.php">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Nein</button>
					<input value="0" type="text" name="dokuid" id="deleteDokuId" style="display: none;">
					<input type="hidden" name="token" value="<?php echo $_SESSION['csrf_token']; ?>">
					<input type="submit" class="btn btn-danger ml-2" value="Löschen" name="deleteDoku">
				</form>
				<!-- <button type="button" class="btn btn-primary">Send message</button> -->
			</div>
		</div>
	</div>
</div>
		
<?php 
if ($showNavigation): ?>
	

	<nav class="navbar navbar-expand-sm bg-secondary navbar-dark">
		<div class="navbar-collapse collapse-xl w-100 order-1 order-md-0 dual-collapse2">
			<a class="navbar-brand" href="./">
				<img href="./" src="assets/images/logo.png" alt="Logo" style="width:40px;">
			</a>
			<a class="navbar-brand" href="./"><?php echo $title; ?></a>
			<ul class="navbar-nav mr-auto">
			
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<i class="fas fa-plus mr-1"></i> Daten hinzufügen
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="./adddoku.php"><i class="fas fa-plus mr-1"></i> Doku Hinzufügen</a>
					   <!--  <a class="dropdown-item" href="./addLanguage.php"><i class="fas fa-plus mr-1"></i> Sprache Hinzufügen</a>
						<a class="dropdown-item" href="./addPlatform.php"><i class="fas fa-plus mr-1"></i> Platform Hinzufügen</a> -->
					</div>
				</li>
				<li>
					<a style="cursor: pointer;" data-toggle="modal" data-target="#FeedbackModal" class="nav-link"><i class="fas fa-comment"></i> Feedback</a>
				</li>
			</ul>
		</div>
		<?php if (!$logged_in): ?>
			<div class="navbar-collapse collapse-xl w-100 order-3 dual-collapse2">
				 <ul class="navbar-nav ml-auto">
					<li class="nav-item">
						<?php 
							if ($darkmode) {
								echo '<a class="nav-link" href="?darkmode=deactivate&token='.$_SESSION['csrf_token'].'"><i class="fas fa-moon mr-1"></i> Darkmode deaktivieren</a>';
							} else {
								echo '<a class="nav-link" href="?darkmode=activate&token='.$_SESSION['csrf_token'].'"><i class="far fa-moon mr-1"></i> Darkmode aktivieren</a>' ;
							}
							// echo $darkmode;
						
						?>
					</li>
					<li>
						<a style="cursor: pointer;" data-toggle="modal" data-target="#LoginModal" class="nav-link"><i class="fas fa-sign-in"></i> Einloggen</a>
					</li>
				</ul>
			</div>
		<?php else: ?>
			<div class="navbar-collapse collapse-xl w-100 order-3 dual-collapse2">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
							<?php echo $_SESSION['username']; ?>
						</a>
						<div class="dropdown-menu dropdown-menu-right">
							<a class="dropdown-item" href="./usercp.php"><i class="fas fa-user"></i> Mein Konto</a>
							<?php if (userIsMod()): ?> <a class="dropdown-item" href="./modcp.php"><i class="fas fa-user-tie"></i> Mod CP</a> <?php endif;?>
							<?php if (userIsAdmin()): ?> <a class="dropdown-item" href="./admincp.php"><i class="fas fa-user-ninja"></i> Admin CP</a> <?php endif;?>
							<div>
								<form method="post">
									<input type="text" style="display: none;" name="token" value="<?php echo $_SESSION['csrf_token'] ?>">
									<button class="dropdown-item" type="submit" name="logoff" style="cursor: pointer;">
										<i class="fas fa-sign-out"></i> Abmelden
									</button>
								</form>
							</div>
						</div>
					</li>
				</ul>
			</div>
		<?php endif; ?>
	</nav>


<?php endif; 

if (isset($_GET['action'])) { ?>

	<div class="container my-2">

	<?php
	if ($_GET['action'] == 'logout') {
		echo '<div class="alert alert-success"><strong>Abgemeldet</strong> Du hast dich erfolgreich Abgemeldet!</div>';
	};
	?></div><?php
}

if (isset($_GET['error'])) {
	if ($_GET['error'] == "dokunotfound") {
		echo '<div class="container-fluid my-4">';
			echoerror('Die doku konnte nicht gefunden werden!', 'Ups!');
		echo '</div>';
	} elseif ($_GET['error'] == "nologin") {
		echo '<div class="container-fluid my-4">';
			echoerror('Du musst dich einloggen um diese seite zu sehen!');
		echo '</div>';
	}  elseif ($_GET['error'] == "nopermission") {
		echo '<div class="container my-4">';
			echoerror('Du hast keine Berechtigung diese seite zu sehen!');
		echo '</div>';
	}
}

if (isset($_GET['success'])) {
	if ($_GET['success'] == "editdoku") {
		echo '<div class="container-fluid my-4">';
			echosuccess('Die doku wurde erfolgreich editiert!');
		echo '</div>';
	}

	if ($_GET['success'] == "adddoku") {

		echo '<div class="container mt-4">';
			echosuccess('Vielen dank! Die Doku wurde erfolgreich in der Datenbank gespeichert');
		echo '</div>';  

	}

	if ($_GET['success'] == "delDoku") {

		echo '<div class="container mt-4">';
			echosuccess('Vielen dank! Die Doku wurde erfolgreich in der Datenbank gelöscht!');
		echo '</div>';  

	}

}

if (isset($_SESSION['banned']) && !$_SESSION['ban_showed']) {
		echo '<div class="container-fluid my-4">';
			echoerror('Du bist leider gebannt! Grund:'.$_SESSION['banreason'],'Banned');
		echo '</div>';
		$_SESSION['ban_showed'] = true;
}

if (isset($feedback)) {
	
	if ($feedback) {
		
		echo '<div class="container-fluid my-4">';
			echosuccess('Vielen dank für dein Feedback!');
		echo '</div>';

	} else {

		echo '<div class="container-fluid my-4">';
			echoerror('Ups! ');
		echo '</div>';

	}

}
?>