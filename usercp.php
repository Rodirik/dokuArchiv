<?php
	
	require 'templates/start.php';

	if (isset($_GET['view'])) {

		if ($_GET['view'] == "settings") {

			$site = "settings";

		} elseif ($_GET['view'] == "favs") {

			$site = "favs";

		} elseif ($_GET['view'] == "liked") {

			$site = "liked";

		} elseif ($_GET['view'] == "seen") {

			$site = "seen";

		} else {

			$site = "settings";

		}

	} else {

		$site = "settings";

	}

?>

<!DOCTYPE html>

<html>

<head>

	<?php include "templates/header.php";?>

</head>

<body>

	<?php include "templates/nav.php";

	if ($site == "settings") :?>



	<div class="modal fade " id="DeleteModal">

        <div class="modal-dialog">

            <div class="modal-content">



                <div class="modal-header">

                    <h4 class="modal-title">Account löschen</h4>

                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>    



                <div class="modal-body">

                    Möchtest du deinen account wirklich löschen?



                    Folgende daten werden sofort von diesem server gelöscht:

                    	<ul class="mt-1">

                    		<li>

                    			Deine markierungen an Dokus (z.B. likes)

                    		</li>

                    		<li>

                    			Deine user ID vom Oauth provider

                    		</li>

                    	</ul>

                    	<p class="mt-1">Diese Daten, sind weiterhin auf anderen Servern in form von Backups bis zu 21 Tage vorhanden.</p>

                </div>



                <div class="modal-footer">

                    <form method="post">

                    	<button type="button" class="btn btn-danger" data-dismiss="modal">Nein, lieber doch nicht!</button>

                    	<input class="btn btn-success" type="submit" name="deleteaccount" value="Ja, ich möchte meinen account löschen!">

                    </form>

                </div>



            </div>

        </div>

    </div>

<?php endif ?>



			<div class="container">



				<ul class="nav nav-tabs nav-justified mt-5">

					<li class="nav-item">

						<a class="nav-link <?php if ($site == 'settings') {echo 'active';} ?>" href="?view=settings"><i class="fas fa-wrench"></i> Einstellungen</a>

					</li>

					<li class="nav-item">

						<a class="nav-link <?php if ($site == 'favs') {echo 'active';} ?>" href="?view=favs"><i class="fas fa-star"></i> Favorisierte Dokus</a>

					</li>

					<li class="nav-item">

						<a class="nav-link <?php if ($site == 'liked') {echo 'active';} ?>" href="?view=liked"><i class="fas fa-angle-up"></i> Gelikte Dokus</a>

					</li>

					<li class="nav-item">

						<a class="nav-link <?php if ($site == 'seen') {echo 'active';} ?>" href="?view=seen"><i class="far fa-eye"></i> Gesehene Dokus</a>

					</li>

				</ul>

			</div>

			<?php

				if ($site == "settings"):

			?>

			<div class="container">

				<?php else: ?>

			<div class="container-fluid">

			<?php endif ?>

			<!-- <div class="card">

				<div class="card-body"> -->

					<?php if ($site == "settings") :?>

			

				<table class="table table-borderless">

					<tr>

						<th>Username:</th>

						<td><p><?php echo $_SESSION['username']; ?></p></td>

					</tr>

					<tr>

						<th>Userid:</th>

						<td><p><?php echo $_SESSION['id']; ?></p></td>

					</tr>

					<tr>
						
						<th>Login via:</th>

						<td><?php echo $_SESSION['loginprovider']; ?></td>

					</tr>

					<!-- <tr>

						<th>Rolle:</th>

						<td><p><?php echo $_SESSION['rolle']; ?></p></td>

					</tr> -->

					<tr>

						<th>

							Darkmode

						</th>

						<td>

							<?php

									if ($_COOKIE[$cookie_prefix.'darkmode'] || $_GET['darkmode'] == 'activate') : ?>

										<a href="?darkmode=deactivate&token=<?php echo $_SESSION['csrf_token'] ?>" class="btn btn-dark">Darkmode Deaktivieren</a>

									<?php else : ?>

										<a href="?darkmode=activate&token=<?php echo $_SESSION['csrf_token'] ?>" class="btn btn-light">Darkmode Aktivieren</a>

								<?php endif; 

								 ?>

						</td>

					</tr>

					<tr>

						<th>Account löschen:</th>

						<td><p><a data-toggle="modal" data-target="#DeleteModal" class="btn btn-danger" href="#">Account löschen</a></p></td>

					</tr>

				</table>

		

		<?php elseif ($site == 'favs') : ?>
			<?php

					$markedDokus = $pdo->prepare('SELECT dokus.*, likes.liked, likes.marked, likes.seen FROM likes LEFT JOIN dokus ON likes.doku_id = dokus.id WHERE likes.user_id = :user_id AND likes.marked = \'1\'');

					$markedDokus->execute(array('user_id' => $_SESSION['id']));

					$markedDokusCount = $markedDokus->rowCount();

					// var_dump($favDokus->fetchAll());

				?>

			

				

					<?php

						while ($markedDoku = $markedDokus->fetch()) { 

							$dokuList[] = array(
								'nr' => $markedDoku['id'],
								'name' => $markedDoku['name'],
								'url' => $markedDoku['url'],
								'length' => $markedDoku['laenge'],
								'language' => $markedDoku['sprache'],
								'tags' => $markedDoku['tags'],
								'platform' => $markedDoku['platform'],
								'hint' => $markedDoku['hint'],
								'liked' => $markedDoku['liked'],
								'marked' => $markedDoku['marked'],
								'seen' => $markedDoku['seen']
							);							

						}

						generateDokuTable($dokuList, $markedDokusCount);

					?>

		<?php elseif ($site == 'liked') : ?>

			<?php

					$favDokus = $pdo->prepare("SELECT dokus.*, likes.liked, likes.marked, likes.seen FROM likes LEFT JOIN dokus ON likes.doku_id = dokus.id WHERE likes.user_id = :user_id AND likes.liked = 1");

					$favDokus->execute(array('user_id' => $_SESSION['id']));

					$favDokusCount = $favDokus->rowCount();

					// var_dump($favDokus->fetch());

				?>

					<?php

						while ($favDoku = $favDokus->fetch()) {


								$dokuList[] = array(
									'nr' => $favDoku['id'],
									'name' => $favDoku['name'],
									'url' => $favDoku['url'],
									'length' => $favDoku['laenge'],
									'language' => $favDoku['sprache'],
									'tags' => $favDoku['tags'],
									'platform' => $favDoku['platform'],
									'hint' => $favDoku['hint'],
									'liked' => $favDoku['liked'],
									'marked' => $favDoku['marked'],
									'seen' => $favDoku['seen']
								);							

	
						}
						generateDokuTable($dokuList, $favDokusCount);

					?>

		<?php elseif ($site == 'seen') : ?>

			<?php

					$seenDokus = $pdo->prepare('SELECT dokus.*, likes.liked, likes.marked, likes.seen FROM likes LEFT JOIN dokus ON likes.doku_id = dokus.id WHERE likes.user_id = :user_id AND likes.seen = \'1\'');

					$seenDokus->execute(array('user_id' => $_SESSION['id']));

					$seenDokusCount = $seenDokus->rowCount();

					// var_dump($favDokus->fetchAll());

				?>


					<?php

						while ($seenDoku = $seenDokus->fetch()) {

							$dokuList[] = array(
								'nr' => $seenDoku['id'],
								'name' => $seenDoku['name'],
								'url' => $seenDoku['url'],
								'length' => $seenDoku['laenge'],
								'language' => $seenDoku['sprache'],
								'tags' => $seenDoku['tags'],
								'platform' => $seenDoku['platform'],
								'hint' => $seenDoku['hint'],
								'liked' => $seenDoku['liked'],
								'marked' => $seenDoku['marked'],
								'seen' => $seenDoku['seen']
							);

							

						}

						generateDokuTable($dokuList, $seenDokusCount);
					?>

	<?php endif ?>

			<!-- </div>

		</div> -->

	</div>

	<?php include "templates/footer.php";?>

</body>

</html>