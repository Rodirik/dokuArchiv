<?php
	
	require 'templates/start.php';

?>

<!DOCTYPE html>

<html>

<head>

	<?php include "templates/header.php";?>

</head>

<body>

	<?php include "templates/nav.php";?>

		<div class="container">

			<div class="card mt-4">
				
				<div class="card-body">
					
					<h3>Dokuarchiv 2.1.1</h3>

					<p>

						So hier ein sehr praktisches feature: Man kann nun dokus ausblenden die man als "gesehen" markiert hat!
						
						<ul>
							
							<li>Man kann nun dokus ausblenden die man als "gesehen" markiert hat</li>

							<li>Kleine Bugfixes und verbesserungen</li>

						</ul>

						Und hier noch ein Hinweis: Wenn ihr euch einloggt werden keine E-mail adressen gespeichert!

						Hier ein auszug aus der Datenbank:

						<img class="mt-3" src="assets/images/sql_user_row.png">

					</p>

				</div>

			</div>

			<div class="card mt-4">
				
				<div class="card-body">
					
					<h3>Dokuarchiv 2.1</h3>

					<p>

						Uuuups! Da war ich wohl so aufgeregt vor dem Release das ich vergessen habe ein paar sachen zu testen :D
						
						<ul>
							
							<li>Dokus editieren ist nun gefixt</li>

							<li>Dokus werden jetzt auch wirklich gelöscht wenn man auf den Button klickt</li>

						</ul>

					</p>

				</div>

			</div>
			
			<div class="card mt-4">
				
				<div class="card-body">
					
					<h3>Dokuarchiv 2.0</h3>

					<p>

						Das Dokuarchiv 2.0 ist eine komplette neuentwicklung, ich habe im zuge dessen die datei struktur und das Datenbank design verbessert. Ihr könnt euch auch schon auf einige weitere änderungen freuen die noch kommen werden!
						
						<ul>
							
							<li>Man kann sich nun via Twitch & Discord einloggen und dokus markieren</li>

							<li>Die Suche wurde verbessert</li>

							<li>Die seiten navigierung wurder verbessert</li>

						</ul>

					</p>

				</div>

			</div>

		</div>

	<?php include "templates/footer.php";?>

</body>

</html>