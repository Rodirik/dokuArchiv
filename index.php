<?php
	
	require 'templates/start.php';

	$query = urlencode($_SERVER['QUERY_STRING']);
	
	$entrys = array();

	// Überprüfung ob besucher eingeloggt ist
	if ($logged_in) {

		// Wenn ja entsprechende Query vorbereiten
		$dokus = "SELECT dokus.*, likes.liked, likes.marked, likes.seen FROM dokus LEFT JOIN likes ON likes.doku_id = dokus.id AND likes.user_id = :userid  WHERE dokus.status = 'visible'";

		$entrys['userid'] = $_SESSION['id'];

	} else {

		$dokus = "SELECT * FROM dokus WHERE status = 'visible'";

	}



	
	// Überprüfung ob die suche ausgefüllt wurde wenn ja alle werte überprüfen und wenn diese NICHT leer sind in array $suche hinzugefügt
	if (isset($_GET['sprache'])) {  

		if (isset($_GET['sprache']) && $_GET['sprache'] != '4') {

			$suche['sprache'] = $_GET['sprache'];

		}

		if (isset($_GET['platform']) && $_GET['platform'] != '0') {

			$suche['platform'] = $_GET['platform'];

		}

		if (isset($_GET['name']) && !empty($_GET['name'])) {

			$suche['name'] = '%' . $_GET['name'] . '%';

		}

		if (isset($_GET['tags']) && !empty($_GET['tags'])) {

			$suche['tags'] = '%' . $_GET['tags'] . '%';

		}

		// Überprüfung ob haken bei "Gesehene dokus ausblenden" gesetzt wurde wenn ja query einsetzen
		if (isset($_GET['seeSeenDokus'])) {

			$dokus .= " AND NOT EXISTS( SELECT * FROM dokus WHERE likes.seen = 1 )";

		}


		// Für jede suche string an sql statement ranhängen
		foreach ($suche as $key => $item) {

			$dokus .= " AND $key LIKE :$key";

			$entrys[$key] = $item;
		}


		// Überprüfung für reigenfolge
		if (isset($_GET['sortorder'])) {
			
			if ($_GET['sortorder'] == '1') {

				$dokus .= " ORDER BY dokus.id ASC";

			} elseif ($_GET['sortorder'] == '2') {
				
				$dokus .= " ORDER BY dokus.id DESC";

			} elseif ($_GET['sortorder'] == '3') {
				
				$dokus .= " ORDER BY dokus.name ASC";

			} elseif ($_GET['sortorder'] == '4') {
				
				$dokus .= " ORDER BY dokus.name DESC";

			}

		}

		
	}


	// Zählung der gesamt ergebnisse
	$ergebnisse = $pdo->prepare($dokus);

	$ergebnisse->execute($entrys);

	$ergebnisse = $ergebnisse->rowCount();

	// Abrufen der aufgerufenen seitenzahl
	if (isset($_GET['site'])) {

		$site = $_GET['site'];

	} else {

		$site = 1;

	}

	// Seitenauswahl vorbereiten
	$paginator = new Paginator($ergebnisse, 50, $site, '?site=(:num)');

	$results = ($site-1) * 50;

	$dokus .= " LIMIT ".$results.", 50";

	$dokus = $pdo->prepare($dokus);

	$dokus->execute($entrys);



?>
<!DOCTYPE html>
<html>
<head>
	<?php require "templates/header.php";?>
	<style type="text/css">
		.unstyled-button {
  			border: none;
  			padding: 0;
  			background: none;
		}
	</style>
</head>
<body>
	
<?php require "templates/nav.php";?>

	<div class="container-fluid">

		<div class="mt-5">
			<h3 style="text-align: center;">Suche</h3>

			<form class="form-inline mx-auto mt-3" action="?sort=true" autocomplete="off" method="get">

				<label class="ml-auto" for="sprache">Sprache: </label>

				<select class="form-control ml-2" name="sprache">
					<option <?php if ($_GET['sprache'] == '4') { echo 'selected=""'; } ?> value="4">Alle</option>
					<option <?php if ($_GET['sprache'] == '0') { echo 'selected=""'; } ?> value="0">DE</option>
					<option <?php if ($_GET['sprache'] == '1') { echo 'selected=""'; } ?> value="1">EN</option>
					<option <?php if ($_GET['sprache'] == '2') { echo 'selected=""'; } ?> value="2">DE/EN</option>
					<option <?php if ($_GET['sprache'] == '3') { echo 'selected=""'; } ?> value="3">Andere</option>
				</select>

				<label class="ml-4" for="platform">Platform: </label>
				<select class="form-control ml-2" name="platform">
					<option <?php if ($_GET['platform'] == '0') { echo 'selected=""'; } ?> value="0">Alle</option>
					<option <?php if ($_GET['platform'] == '1') { echo 'selected=""'; } ?> value="1">Youtube</option>
					<option <?php if ($_GET['platform'] == '2') { echo 'selected=""'; } ?> value="2">Netflix</option>
					<option <?php if ($_GET['platform'] == '3') { echo 'selected=""'; } ?> value="3">Andere</option>
				</select>

				<label class=" ml-4" for="name">Name:</label>
				<input <?php echo 'value="'.htmlentities($_GET['name']).'"'; ?> type="text" class="form-control ml-2" name="name">

  				<label class=" ml-4" for="thema">Tags:</label>
  				<input <?php echo 'value="'.htmlentities($_GET['tags']).'"'; ?> type="text" class="form-control ml-2" name="tags">

  				<label class="ml-4" for="sortorder">Sortierung: </label>
				<select class="form-control ml-2" name="sortorder">
					<option <?php if ($_GET['sortorder'] == '1') { echo 'selected=""'; } ?> value="1">Älteste</option>
					<option <?php if ($_GET['sortorder'] == '2') { echo 'selected=""'; } ?> value="2">Neueste</option>
					<option <?php if ($_GET['sortorder'] == '3') { echo 'selected=""'; } ?> value="3">Alphabetisch (A-Z)</option>
					<option <?php if ($_GET['sortorder'] == '4') { echo 'selected=""'; } ?> value="4">Alphabetisch (Z-A)</option>
					<!-- <option <?php if ($_GET['sortby'] == 'mostLiked') { echo 'selected=""'; } ?> value="1">Meiste likes</option> -->
					<!-- <option <?php if ($_GET['sortby'] == '2') { echo 'selected=""'; } ?> value="2">Netflix</option>
					<option <?php if ($_GET['sortby'] == '3') { echo 'selected=""'; } ?> value="3">Andere</option> -->
				</select>

				<?php if ($logged_in): ?>
				<label class="form-check-label ml-4">
    				<input type="checkbox" class="form-check-input" name="seeSeenDokus" <?php if (isset($_GET['seeSeenDokus'])) { echo 'checked=""'; } ?>>Gesehene Dokus ausblenden
  				</label>
  			<?php endif; ?>

  				<button type="submit" class="btn btn-primary ml-4 mr-auto">Suchen</button>

			</form> 

		</div>

		<div class="mt-5" style="text-align: center;">

			<h3>Dokus</h3>

		</div>
			<?php

				// Vorbereitung für die zählung der likes für jede doku
				$dokuLikes = $pdo->prepare("SELECT count(*) FROM likes WHERE doku_id = ? AND liked = 1");

				// seiten buttons ausgeben
				echo $paginator;

			 ?> 
	<div class="mt-4">

		<?php

			// Vor ausgabe der Dokus überprüfung ob der Besucher eingeloggt ist
			if ($logged_in) {
				

				while ($doku = $dokus->fetch()) {
	
					$dokuList[] = array(
						'nr' => $doku['id'],
						'name' => $doku['name'],
						'url' => $doku['url'],
						'length' => $doku['laenge'],
						'language' => $doku['sprache'],
						'tags' => $doku['tags'],
						'platform' => $doku['platform'],
						'hint' => $doku['hint'],
						'liked' => $doku['liked'],
						'marked' => $doku['marked'],
						'seen' => $doku['seen']
					);
				}

			} else {

				while ($doku = $dokus->fetch()) {
	
					$dokuList[] = array(
						'nr' => $doku['id'],
						'name' => $doku['name'],
						'url' => $doku['url'],
						'length' => $doku['laenge'],
						'language' => $doku['sprache'],
						'tags' => $doku['tags'],
						'platform' => $doku['platform'],
						'hint' => $doku['hint']
					);
				}

			}

			$options = array('query' => $query, );

			
			// Tabelle ausgeben
			generateDokuTable($dokuList, $ergebnisse, $options);

		?>

		</div>

		<?php

			echo $paginator;

		?>

	</div>	

<?php require "templates/footer.php";

if ($logged_in) :

?>



<?php else : ?>

<script type="text/javascript">

	$(document).ready(function(){

		// Wenn ein nicht eingeloggter user z.B. auf "Markieren" klickt Login modal öffnen

		$('.login').on('click', function(){

			$('#LoginModal').modal('show');

		});

	});

</script>

<?php endif ?>

</body>

</html>