<?php

	require 'templates/start.php';

	// Datenbank abfrage vorbereiten

	$doku = $pdo->prepare("SELECT * FROM dokus WHERE id = ? AND status = 'visible'");

	$doku->execute(array($_GET['dokuid']));

// Wenn es die doku nicht gibt umleiten auf index.php
	if (!$doku = $doku->fetch()) {

		header('Location: ./index.php?'.$_GET['lastquery'].'&error=dokunotfound');

	}


	// Überprüfen ob die Doku gelöscht werden soll
	if (isset($_GET['deleteDoku'])) {

		// Wenn ja csrf token überprüfen
		if ($_GET['token'] == $_SESSION['csrf_token']) {

			// Wenn csrf token korrekt doku als gelöscht markieren und auf index.php umleiten
			$delDoku = $pdo->prepare("UPDATE dokus SET status = 'deleted' WHERE id = :dokuid");

			$delDoku->execute(array('dokuid' => $_GET['dokuid']));

			header('Location: ./index.php?success=delDoku');

			exit();

		} else {

			header('Location: ./index.php?error=nosession');

			exit();

		}

	}

	// array(10) { ["name"]=> string(4) "1337" ["link"]=> string(4) "1337" ["dauer"]=> string(4) "1337" ["tags"]=> string(4) "tags" ["hint"]=> string(5) ":hint" ["sprache"]=> string(1) "0" ["platform"]=> string(1) "1" ["dokuid"]=> string(1) "1" ["token"]=> string(23) "5c460f6fd2aa86.84784522" ["edit"]=> string(7) "Ändern" } 

	// Überprüfung ob daten gesendet wurden
	if (isset($_POST['edit'])) {

		// Wenn ja csrf token überprüfen
		if ($_POST['token'] == $_SESSION['csrf_token'] ) {

			// Eingaben überprüfen
			if (!isset($_POST['name'])) {

				$error[] = 'Die Doku muss einen Namen haben!';

			}

			if (!isset($_POST['link'])) {

				$error[] = 'Die Doku muss einen Link haben!';

			}

			if (!isset($_POST['dauer'])) {

				$error[] = 'Du musst die länger der Doku angeben!';

			}

			if (!isset($_POST['dauer'])) {

				$error[] = 'Du musst die länge der Doku angeben!';

			}

			// Wenn keine Fehler vorliegen strings konvetieren und daten in db ändern
			if (empty($error)) {

				$doku_name = htmlentities($_POST['name']);
				$doku_url = htmlentities($_POST['link']);
				$doku_length = htmlentities($_POST['dauer']);
				$doku_tags = htmlentities($_POST['tags']);
				$doku_hint = htmlentities($_POST['hint']);
				$doku_language = htmlentities($_POST['sprache']);
				$doku_platform = htmlentities($_POST['platform']);
				$doku_dokuid = htmlentities($_POST['dokuid']);


				$editDoku = $pdo->prepare("UPDATE dokus SET name = :name, url = :url, laenge = :laenge, tags = :tags, hint = :hint, sprache = :sprache, platform = :platform WHERE id = :dokuid");

				if ($editDoku->execute(

					array(
						'name' => $doku_name,
						'url' => $doku_url,
						'laenge' => $doku_length,
						'tags' => $doku_tags,
						'hint' => $doku_hint,
						'sprache' => $doku_language,
						'platform' => $doku_platform,
						'dokuid' => $doku_dokuid

					))
				) {
					
					$redirect = './index.php?';

					if (!empty($_GET['lastquery'])) {
						
						$redirect .= $_GET['lastquery'].'&success=editDoku#'.$doku_dokuid;

					} else {

						$redirect .= 'success=editDoku#'.$doku_dokuid;

					}

					header('Location: '.$redirect);

					exit();

				}


			}

		} else {

			// Wenn ungültiger csrf token error ausgeben
			$error[] = 'Deine session ist abgelaufen! Bitte gebe die Änderungen erneut ein!';

		}
		
	}

	// var_dump($doku);

?>

<!DOCTYPE html>

<html>

<head>

	<?php include "templates/header.php";?>

</head>

<body>

	<?php include "templates/nav.php";?>

	<div class="container">

		<div class="mt-5">

			<form method="post" autocomplete="off">

				<div class="form-group">

					<label>Name: <font color="red">*</font></label>

						<input class="form-control" required="" type="text" name="name" maxlength="250" autofocus="" value="<?php echo $doku['name']?>">

				</div>

				<div class="form-group">

					<label>Link: <font color="red">*</font></label>

						<input class="form-control" required="" type="text" name="link" maxlength="250" autofocus="" value="<?php echo $doku['url']?>">

				</div>

				<div class="form-group">

					<label>Dauer: <font color="red">*</font></label>

						<input class="form-control" required="" type="text" name="dauer" maxlength="250" autofocus="" value="<?php echo $doku['laenge']?>">

				</div>

				<div class="form-group">

					<label>Tags: <font color="red">*</font></label>

						<input class="form-control" required="" type="text" name="tags" maxlength="250" autofocus="" value="<?php echo $doku['tags']?>">

				</div>

				<div class="form-group">

					<label>Thema/Hinweise:</label>

						<input class="form-control" type="text" name="hint" maxlength="250" autofocus="" value="<?php echo $doku['hint']?>">

				</div>

				<div class="form-group">

					<label>Sprache: <font color="red">*</font></label>

						<select class="form-control" name="sprache">

							<option <?php if ($doku['sprache'] == '0') { echo 'selected=""'; } ?> value="0">DE</option>

							<option <?php if ($doku['sprache'] == '1') { echo 'selected=""'; } ?> value="1">EN</option>

							<option <?php if ($doku['sprache'] == '2') { echo 'selected=""'; } ?> value="2">DE/EN</option>

							<option <?php if ($doku['sprache'] == '3') { echo 'selected=""'; } ?> value="3">Andere</option>

						</select>

				</div>

				<div class="form-group">

					<label>Platform: <font color="red">*</font></label>

						<select class="form-control" name="platform">

							<option <?php if ($doku['platform'] == '1') { echo 'selected=""'; } ?> value="1">Youtube</option>

							<option <?php if ($doku['platform'] == '2') { echo 'selected=""'; } ?> value="2">Netflix</option>

							<option <?php if ($doku['platform'] == '3') { echo 'selected=""'; } ?> value="3">Andere</option>

						</select>

				</div>

				<input type="hidden" name="dokuid" value="<?php echo $doku['id'] ?>">

				<input type="hidden" name="token" value="<?php echo $_SESSION['csrf_token'] ?>">

				<div class="form-group mb-5">

					<input type="submit" class="btn btn-success" name="edit" value="Ändern">

					<input type="reset" class="btn btn-danger">

					<a class="btn btn-secondary" href="./index.php?<?php echo $_GET['lastquery'] ?>#<?php echo $_GET['dokuid'] ?>" >Zurück</a>

				</div>

			</form>

		</div>

	</div>

	<?php include "templates/footer.php";?>

</body>

</html>