<?php
	

	require 'templates/start.php';



	// Überprüfung welche seite aufgerufen wurde
	if (isset($_GET['view'])) {

		if ($_GET['view'] == "newDokus") {

			$site = "newDokus";

		} elseif ($_GET['view'] == "editedDokus") {

			$site = "editedDokus";

		} elseif ($_GET['view'] == "userAccs") {

			$site = "userAccs";

		} elseif ($_GET['view'] == "deleted") {

			$site = "deleted";

		} else {

			$site = "settings";

		}

	} else {

		$site = "newDokus";

	}

?>

<!DOCTYPE html>

<html>

<head>

	<?php include "templates/header.php";?>

</head>

<body>

	<?php include "templates/nav.php";?>



	<div class="container">



		<ul class="nav nav-tabs nav-justified mt-5">



			<li class="nav-item">

				<a class="nav-link <?php if ($site == 'newDokus') {echo 'active';} ?>" href="?view=newDokus"><i class="far fa-file-alt"></i> Neueste Dokus</a>

			</li>



			<li class="nav-item">

				<a class="nav-link disabled <?php if ($site == 'editedDokus') {echo 'active';} ?>" href="?view=editedDokus" style="pointer-events: none;"><i class="far fa-file-edit"></i></i> Bearbeitete dokus</a>

			</li>



			<li class="nav-item">

				<a class="nav-link disabled <?php if ($site == 'userAccs') {echo 'active';} ?>" href="?view=userAccs" style="pointer-events: none;"><i class="fas fa-users"></i> User accounts</a>

			</li>



			<li class="nav-item">

				<a class="nav-link disabled <?php if ($site == 'deleted') {echo 'active';} ?>" href="?view=deleted" style="pointer-events: none;"><i class="far fa-file-minus"></i> Gelöschte Dokus</a>

			</li>



		</ul>



	</div>

	<div class="container-fluid">

		<?php if ($site == "newDokus"):



			$getLastDokus = $pdo->prepare("SELECT * FROM dokus WHERE status = 'visible' ORDER BY id DESC LIMIT 50");

			$getLastDokus->execute(array());

			$getLastDokusCount = $getLastDokus->rowCount();

			// var_dump($getLastDokus);

			?>

					<?php

						while ($getLastDoku = $getLastDokus->fetch()) { 

							$dokuList[] = array(
								'nr' => $getLastDoku['id'],
								'name' => $getLastDoku['name'],
								'url' => $getLastDoku['url'],
								'length' => $getLastDoku['laenge'],
								'language' => $getLastDoku['sprache'],
								'tags' => $getLastDoku['tags'],
								'platform' => $getLastDoku['platform'],
								'hint' => $getLastDoku['hint'],
								'liked' => $getLastDoku['liked'],
								'marked' => $getLastDoku['marked'],
								'seen' => $getLastDoku['seen']
							);							

						}

						generateDokuTable($dokuList, $getLastDokusCount);


					?>



		<?php elseif ($site == "editedDokus"): 



			$editedDokus = $pdo->prepare("SELECT * FROM edits ORDER BY id DESC");

			$editedDokus->execute(array());

			?>



			<table class="table table-striped table-bordered mt-2">

					<tr class="">

						<th>Name</th>

						<th>Dauer</th>

						<th>Sprache</th>

						<th>Tags</th>

						<th>Platform</th>

						<th>Thema/Hinweise</th>

						<th>Aktionen</th>

					</tr>

					<?php

						while ($editedDoku = $editedDokus->fetch()) { 

							$editedDoku['old'] = json_decode($editedDoku['old'], true);

							$editedDoku['new'] = json_decode($editedDoku['new'], true);



							$editedDoku['new']['sprache'] = getLanguage($editedDoku['new']['sprache']);



							echo "Sprache: " . $editedDoku['new']['sprache'];





							if ($editedDoku['platform'] == '1') {

								$platform = "Youtube"; 

							} elseif ($editedDoku['platform'] == '2') {

								$platform = "Netflix";

							} elseif ($editedDoku['platform'] == '3') {

								$platform = "Andere";

							} else { 

								$platform = "Andere";

							}

							echo '

								<tr>



									<td><a rel="noopener" target="_blank" name="'.$editedDoku['name'].'" href='.$editedDoku['url'].'">'.$editedDoku['name'].'
									</a></td>



									<td>'.$editedDoku['laenge'].'</td>



									<td>'.$sprache.'</td>



									<td>'.$editedDoku['tags'].'</td>



									<td>'.$platform.'</td>



									<td>'.$editedDoku['hint'].'</td>



									<td>

										<div>

											Ich bin nur ein platzhalter staiyM

										</div>

									</td>

								</tr>
				

							';

						}

					?>

			</table>



		<?php elseif ($site == "userAccs"): ?>



		<?php elseif ($site == "deleted"): ?>



		<?php endif; ?>

		

	</div>



	<?php include "templates/footer.php";?>

</body>

</html>