 <?php
/**
* Die Login-Klasse
*/
class Login
   {
   //Hat den Wert 0 oder 1, on oder off ;)
   private $status;

   //Den Namen der Session und des Cookies fest definieren
   const SNAME = 'LOGGED_IN';

   
   /**
   * Der Konststruktur, er wird bei der Erzeugung
   * des Objekts aufgerufen. Prüft auf Sessions und
   * Cookies und prüft die GET-Variable "action"
   */
   public function __construct ($cookie_prefix)
      {

      //Ist eine Session oder ein Cookie gesetzt?
      if(isset($_SESSION[self::SNAME]))
         $this->status = 1;
      else
         $this->status = 0;



      //Erstmal ganz allgemein prüfen,
      //ob sich die GET-Variable in der URL befindet
      if(isset($_GET['action']))
         {
         //Wert in einer Variable ablegen
         $action = $_GET['action'];

         //Wenn der Wert "login" ist...
         if($action == 'login')
            {
            //...prüfen, ob die POST-Variable "dauerhaft" dabei ist,
            //um ggf. das Cookie zu setzen. Diese POST-Variable
            //stammt von einer Check-Box ;)
            if(isset($_POST['dauerhaft']))
               $use_cookie = true;
            else
               $use_cookie = false;

            //Die Methode login aufrufen und einloggen,
            //evtl. auch das Cookie benutzen
            $this->login($use_cookie);
            }
         //Oder der Wert ist "logout", dann...
         else if($action == 'logout')
            {
            //... wird sich ausgeloggt, indem die
            //entsprechende Methode aufgerufen wird
            $this->logout();
            }
         }
      } //Konstruktor ENDE


   /**
   * login()
   * @param  boolean  $use_cookie
   * Setzt die Session und ggf. auch das Cookie
   */
   public function  login ($use_cookie=false)
      {
      //Ist der User momentan nicht eingeloggt? Dann...
      if(!$this->logged_in())
         {
         //Status auf 1 (On) setzen
         $this->status = 1;
         //Session setzen
         //Zugriff auf die Konstante per self::SNAME
         $_SESSION[self::SNAME] = true;

         //Falls gegeben, Cookie setzen
         if($use_cookie)
            //Die Laufzeit beträgt eine Woche
            //60s * 60 = 1 Stunde
            //1 Stunde * 60 = 1 Tag
            //1 Tag * 7 = 1 Woche
            setcookie(self::SNAME, true, (time()+60*60*24*7));
         session_regenerate_id();
         }
      }


   /**
   Prüft einfach bloß die Eigenschaft und
   stellst dadurch off der on fest
   */
   public function logged_in ()
      {
      if($this->status == 1) {
         return true;
      } else
         return false;
      }


   /**
   * Setzt den Status auf 1 (Off) und
   löscht ggf. die Session oder das Cookie
   */
   public function logout ()
      {
      if($this->logged_in())
         {
         $this->status = 0;

         $_SESSION = array();

         session_regenerate_id();


         if(isset($_SESSION[self::SNAME]))
            unset($_SESSION[self::SNAME]);

         }
      }
   } //Login-Kasse ENDE
?>
