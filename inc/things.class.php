<?php


function timetostr($time)
{

	if (!isset($time)) {
		$time = time();
	}
	$string = date('Y-m-d H:i:s',$time);

	return $string;
};

function userIsMod()
{

	// Rollen abfragen
	$mod = $GLOBALS['mod'];

	// Rollen die zugriff aufs ModCP haben
	$mod = array('mod', 'admin');
	
	if (in_array($_SESSION['rolle'], $mod)) {
		return true;
	} else {
		return false;
	}
}

function userIsAdmin()
{

	// Rollen abfragen
	$admin = $GLOBALS['admin'];

	// Rollen die zugriff aufs AdminCP haben
	$admin = array('admin');

	if (in_array($_SESSION['rolle'], $admin)) {
		return true;
	} else {
		return false;
	}
}

function timetodate($time,$format='Y-m-d H:i:s')
{

	if (!isset($time)) {
		$time = time();
	}
	$string = date($format,$time);

	return $string;
};

function wartung()
{
	if (file_exists('../wartung.txt')) {
		$wartung = true;
	} else {
		$wartung = false;
	}
	return $wartung;
	;
}

function writeLog($logentry)
{
	if (isset($logentry)) {
		$time = timetostr();
		$logentry = $time."|".$logentry;
		$logfile = fopen("log.csv", "a+");
    	fwrite($logfile, $logentry);
		fwrite($logfile, "\n\r");
		fclose($logfile);
		return true;
	} else {
		return false;
	}
}

function generateDokuTable($dokus, $dokucount, $options="")
{

	$pdo = $GLOBALS['pdo'];

	$logged_in = $GLOBALS['logged_in'];
	
	echo '
		<table class="table table-responsive-md table-bordered table-striped">

			<thead>

				<tr>
	
					<th style="width: 2,5px;">Nr.</th>
	
					<th>Name</th>
	
					<th>Dauer</th>
	
					<th style="width: 20px;">Sprache</th>
	
					<th style="width: 10px;">Tags</th>
	
					<th>Platform</th>
	
					<th style="width: 400px;">Thema/Hinweise</th>
	
					<th style="width: 10px;"><i class="fas fa-chevron-up"></i></th>
	
					<th style="width: 145px;"></th>
	
				</tr>

			</thead>
	';

	echo '

		<tbody>

	';

	if ($dokucount == 0) {
		
		echo '<tr><td style="text-align: center;" colspan="9">Für deine Suchanfrage wurden leider keine passenden ergebnisse gefunden :(</td></tr>';

	} else {

		$dokuLikes = $pdo->prepare("SELECT count(*) FROM likes WHERE doku_id = ? AND liked = 1");

		foreach ($dokus as $doku) {

			if (!$logged_in) {

				$dokuButton['like'] = '<span data-toggle="tooltip" style="cursor: pointer; color: gray;" class="login fas fa-chevron-up" title="Du musst dich anmelden um eine Doku zu liken!"></span>';

				$dokuButton['mark'] = '<span data-toggle="tooltip" style="cursor: pointer; color: gray;" class="login ml-1 far fa-star" title="Du musst dich anmelden um eine Doku zu 
					liken!"></span>';

				$dokuButton['seen'] = '<span data-toggle="tooltip" style="cursor: pointer; color: gray;" class="login ml-1 far fa-eye" title="Du musst dich anmelden um eine Doku zu liken!"></span>';
				
				$dokuButton['edit'] = '<a data-toggle="tooltip" class="ml-1" title="Doku editieren" href="./edit.php?dokuid='.$doku['nr'].'&lastquery='.$options['query'].'"><i class="fas fa-pencil-alt"></i></a>';

				$dokuButton['del'] = '<span data-toggle="tooltip" style="cursor: pointer; color: red;" class="ml-1 far fa-trash-alt delete" data-id="'.$doku['nr'].'" data-name="'.$doku['name'].'" title="Doku löschen"></span>';

				} else {

					if ($doku['liked'] == '1') {
				
					$dokuButton['like'] = '<span data-toggle="tooltip" style="cursor: pointer; color: darkcyan;" class="unlike ml-1 fas fa-chevron-up" data-id="'.$doku['nr'].'" title="Doku 
						unliken"></span>';

				} else {

					$dokuButton['like'] = '<span data-toggle="tooltip" style="cursor: pointer; " class="like ml-1 fas fa-chevron-up" data-id="'.$doku['nr'].'" title="Doku liken"></span>';

				}


				if ($doku['marked'] == '1') {
				
					$dokuButton['mark'] = '<span data-toggle="tooltip" tooltip="Doku markieren" style="cursor: pointer; color: gold;" class="unmark ml-1 fas fa-star" data-id="'.$doku['nr'].'" title="Doku unmarkieren"></span>';

				} else {

					$dokuButton['mark'] = '<span data-toggle="tooltip" style="cursor: pointer; color: gold;" class="marked ml-1 far fa-star" data-id="'.$doku['nr'].'" title="Doku 
						markieren"></span>';

				}


				if ($doku['seen'] == '1') {
				
					$dokuButton['seen'] = '<span data-toggle="tooltip" style="cursor: pointer; color: lightblue;" class="unseen ml-1 far fa-eye-slash" data-id="'.$doku['nr'].'" title="Doku 
						als ungesehen Markieren"></span>';

				} else {

					$dokuButton['seen'] = '<span data-toggle="tooltip" style="cursor: pointer; color: lightblue;" class="seen ml-1 far fa-eye" data-id="'.$doku['nr'].'" title="Doku als gesehen Markieren"></span>';

				}

					$dokuButton['edit'] = '<a data-toggle="tooltip" class="ml-1" title="Doku editieren" href="./edit.php?dokuid='.$doku['nr'].'&lastquery='.$options['query'] .'"><i class="fas fa-pencil-alt"></i></a>';

					$dokuButton['del'] = '<span data-toggle="tooltip" style="cursor: pointer; color: red;" class="ml-1 far fa-trash-alt delete" data-id="'.$doku['nr'].'" data-name="'.$doku['name'].'" title="Doku löschen"></span>';

			}

			$dokuLikes->execute(array($doku['nr']));

			$dokuLikesCount = $dokuLikes->fetchColumn();

			$doku['language'] = getLanguage($doku['language']);

			$doku['platform'] = getPlatform($doku['platform']);
			
			echo '

				<tr>

					<td id="'.$doku['nr'].'">'.$doku['nr'].'</td>

					<td><a rel="nofollow noopener" title="'.$doku['url'].'" data-placement="right" data-toggle="tooltip" href="'.$doku['url'].'">'.$doku['name'].'</a></td> 

					<td>'.$doku['length'].'</td>

					<td>'.$doku['language'].'</td>

					<td>'.$doku['tags'].'</td>

					<td>'.$doku['platform'].'</td>

					<td>'.$doku['hint'].'</td>

					<td> <span id="likes_count_'.$doku['nr'].'">'.$dokuLikesCount.'</span></td>

					<td> '.$dokuButton['like'].$dokuButton['mark'].$dokuButton['seen'].$dokuButton['edit'].$dokuButton['del'].'

				</tr>

			' ;

		}

	}

	echo '</tbody>
	</table>';



}


function echoerror($msg='Es ist ein unbekannter Fehler aufgetreten!',$title='Fehler!')
{
	echo '<div class="alert alert-danger"><strong>'.$title.'</strong> '.$msg.'</div>';
};

function echowarning($msg, $title='Hinweis!')
{
	echo '<div class="alert alert-warning"><strong>'.$title.'</strong> '.$msg.'</div>';
};

function echosuccess($msg, $title='Aktion erfolgreich!')
{
	echo '<div class="alert alert-success"><strong>'.$title.'</strong> '.$msg.'</div>';
};

function echoinfo($msg, $title='Info!')
{
	echo '<div class="alert alert-info"><strong>'.$title.'</strong> '.$msg.'</div>';
};

function isAdmin()
{
	#d
};

function makeArray($username,$id,$provider)
{
	return( array('username' => $username, 'id' => $id, 'provider' => $provider) );
}


function getLanguage($sprache='-1')
{
	if ($sprache == '0') {
		return("DE"); 
	} elseif ($sprache == '1') {
		return("EN"); 
	} elseif ($sprache == '2') {
		return("DE/EN"); 
	} elseif ($sprache == '3') {
		return("Andere"); 
	} else {
		return("-"); 
	}
}

function getPlatform($platform='')
{
	if ($platform == '1') {
		return("Youtube"); 
	} elseif ($platform == '2') {
		return("Netflix");
	} elseif ($platform == '3') {
		return("Andere");
	} else { 
		return("Andere");
	}
}


?>