<?php



	require 'templates/start.php';



	// if ($logged_in) {

	// 	header('Location: ./index.php');

	// 	exit();

	// }



	$datareturned = false;



    if (isset($_SESSION['banned']) && $_SESSION['banned']) {

        header('Location: ./index.php');

        $_SESSION['ban_showed'] = false;

        exit();

    }



if ($_GET['provider'] == 'discord') {



	$provider = new \Wohali\OAuth2\Client\Provider\Discord([

    	'clientId' => $provider_discord['clientID'],

    	'clientSecret' => $provider_discord['clientSecret'] ,

    	'redirectUri' => $provider_discord['redirectUri']

	]);



	$options = [

    	'scope' => $provider_discord['scope'] // array or string

	];



	if (!isset($_GET['code'])) {



    	// Step 1. Get authorization code

    	$authUrl = $provider->getAuthorizationUrl($options);

    	$_SESSION['oauth2state'] = $provider->getState();

    	header('Location: ' . $authUrl);



		// Check given state against previously stored one to mitigate CSRF attack

	} elseif (empty($_GET['state']) || ($_GET['state'] !== $_SESSION['oauth2state'])) {



    	unset($_SESSION['oauth2state']);

    	exit('Invalid state');



	} else {



    	// Step 2. Get an access token using the provided authorization code

    	$token = $provider->getAccessToken('authorization_code', [

    	    'code' => $_GET['code']

    	]);

    	// Step 3. (Optional) Look up the user's profile with the provided token

    	try {



    	    $user = $provider->getResourceOwner($token);



        	// echo '<h2>Resource owner details:</h2>';

        	// printf('Hello %s#%s!<br/><br/>', $user->getUsername(), $user->getDiscriminator());

        	$user = $user->toArray();



        	$userdata = makeArray($user['username'], $user['id'], 'discord');



        	// var_dump($user);

        	$datareturned = true;



    	} catch (Exception $e) {



        	// Failed to get user details

        	exit('Oh dear...');



    	}

	}

} elseif ($_GET['provider'] == 'twitch') {

	$provider = new \Depotwarehouse\OAuth2\Client\Twitch\Provider\Twitch([

    	'clientId' => $provider_twitch['clientID'],

        'clientSecret' => $provider_twitch['clientSecret'],

        'redirectUri' => $provider_twitch['redirectUri']

	]);



	$options = [

    	'scope' => $provider_twitch['scope']

	];



	if (!isset($_GET['code'])) {



    	// 1. Code abfragen

    	$authUrl = $provider->getAuthorizationUrl($options);

    	$_SESSION['oauth2state'] = $provider->getState();

    	header('Location: ' . $authUrl);



		// Eigenen mit zurückgegebenem CSRF token vergleichen

	} elseif (empty($_GET['state']) || ($_GET['state'] !== $_SESSION['oauth2state'])) {



    	unset($_SESSION['oauth2state']);

    	exit('Invalid state');



	} else {



    	// 2. Access token mit code erfragen

    	$token = $provider->getAccessToken('authorization_code', [

    	    'code' => $_GET['code']

    	]);




    	// 3. Userdaten abfragen

    	try {





    	    $user = $provider->getResourceOwner($token);



        	echo '<h2>Resource owner details:</h2>';

        	$userdata = makeArray($user->getDisplayName(),  $user->getId(), 'twitch');

        	$datareturned = true;



    	} catch (Exception $e) {



        	// Failed to get user details

        	exit('Es ist ein fehler beim Abfragen deiner daten aufgetreten!');



    	}

	}

}



if ($datareturned) {
    

	$userSuchen = $pdo->prepare("SELECT * FROM users WHERE provider_id = ? AND provider = ?");

    $userSuchen->execute(array($userdata['id'], $userdata['provider']));



    $dbData = $userSuchen->fetch();

	if ($dbData) { 



        // echo "Häääääääh";



        if (!$dbData['banned']) {

            $login->login();



            // var_dump($dbData);



            $_SESSION['id'] = $dbData['userid'];

            $_SESSION['username'] = $dbData['username'];

            $_SESSION['provider'] = $dbData['provider'];

            $_SESSION['rolle'] = $dbData['rolle'];

            $_SESSION['loginprovider'] = $dbData['provider'];



        } else {

            $_SESSION['banned'] = true;

            $_SESSION['banreason'] = $dbData['ban_reason'];

            $_SESSION['ban_showed'] = false;

        }

        

	} else {



		if (isset($_COOKIE[$cookie_prefix.'darkmode'])) {

			$darkmode = true;



		} else {

			$darkmode = false;

		}



		$addUser = $pdo->prepare("INSERT INTO users (username, provider_id, provider, darkmode) VALUES (?, ?, ?, ?)");

		$addUser->execute(array($userdata['username'], $userdata['id'], $userdata['provider'], $darkmode));



		$login->login();



        $userSuchen = $pdo->prepare("SELECT * FROM users WHERE provider_id = ? AND provider = ?");

        $userSuchen->execute(array($userdata['id'], $userdata['provider']));



        $dbData = $userSuchen->fetch();



        // var_dump($dbData);



        // echo "Häääh";



        $_SESSION['id'] = $dbData['userid'];

        $_SESSION['username'] = $dbData['username'];

        $_SESSION['loginprovider'] = $dbData['provider'];

	}

	header('Location: ./index.php');

} 
	?>