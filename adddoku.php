<?php
	
	require 'templates/start.php';


	#Überprüfung ob Formular gesendet wurde
	if (!empty($_POST)) {

		// Überprüfung ob CSRF token gesetzt ist und Übereinstimmt
		if (isset($_SESSION['csrf_token']) && isset($_POST['token'])) {
			if ($_SESSION['csrf_token'] != $_POST['token']) {
				$error[] = "Fehlerhafter CSRF-Token!";
	 		}
		} else {
			$error[] = "Kein CSRF-Token vorhanden!";
		}

		// array(9) { ["name"]=> string(4) "1334" ["link"]=> string(15) "https://1337.de" ["dauer"]=> string(2) "01" ["tags"]=> string(3) "bla" ["hint"]=> string(0) "" ["sprache"]=> string(1) "0" ["platform"]=> string(1) "1" ["token"]=> string(23) "5c22587453ec35.85762738" ["edit"]=> string(16) "Doku hinzufügen" } 

		#Wurde ein Name eingegeben?
		if (!isset($_POST['name'])) {
			$error['noName'] = "Du hast keinen Namen für die Doku angegeben!";
		}

		// Wurde ein Gültiger link angegeben?
		if (!isset($_POST['link']) && !filter_var($_POST['link'], FILTER_VALIDATE_URL)) {
			$error['noLink'] = "Du hast keinen oder keinen gültigen link angegeben!";
		}

		// Wurde die Dauer angegeben
		if (!isset($_POST['dauer'])) {
			$error['noLength'] = "Du musst die dauer der Doku angeben!";
		}

		// Wurden tags gesetzt?
		if (empty($_POST['tags'])) {
			$error['noTags'] = "Bitte beschreibe in kurzen stichpunkten den Inhalt der doku!";
		}

		// Sind KEINE fehler aufgetreten?
		if (!isset($error)) {
	
			$doku['name'] = htmlspecialchars($_POST['name']);
			$doku['link'] = htmlspecialchars($_POST['link']);
			$doku['dauer'] = htmlspecialchars($_POST['dauer']);
			$doku['tags'] = htmlspecialchars($_POST['tags']);
			$doku['hint'] = htmlspecialchars($_POST['hint']);
			$doku['sprache'] = htmlspecialchars($_POST['sprache']);
			$doku['platform'] = htmlspecialchars($_POST['platform']);

			// Doku in db schreiben
			$neueDoku = $pdo->prepare("INSERT INTO dokus (name, url, sprache, laenge, tags, hint, platform) VALUES (?, ?, ?, ?, ?, ?, ?)");
			if ($neueDoku->execute(array($doku['name'], $doku['link'], $doku['sprache'], $doku['dauer'], $doku['tags'], $doku['hint'], $_POST['platform']))) {
				header('Location: ?success=adddoku');
				exit();
			} else {
				// Wenn datenbank abfrage = false grund überprüfen und entsprechende fehlermeldung ausgeben

				$neueDokuError = $neueDoku->errorInfo();

				if ($neueDokuError['0'] == '23000') {
					
					$error[] = 'Diese doku ist bereits im Archiv!';

				} else {

					$error[] = $neueDokuError['2'];

				}
				
			}
		}

	}	

?>

<!DOCTYPE html>

<html>

<head>

	<?php include "templates/header.php";?>

</head>

<body>

	<?php include "templates/nav.php";?>

	<div class="container mt-3">
		


		<?php
		// Alle aufgetretenen Fehler ausgeben
			foreach ($error as $fail) {
			
				echoerror($fail);

			}

		?>

	</div>

	<div class="container mt-5">

		<form method="post" autocomplete="off">

			<?php // Wenn eingaben bereits gemacht wurden und es fehler gab werden ensprechende bereiche rot und die werte werden wieder eingetragen ?>

			<div class="form-group" <?php if (isset($error['noName'])): ?> style="color: red;" <?php endif; ?>>

				<label>Name: <font color="red">*</font></label>

					<input class="form-control" required="" type="text" name="name" maxlength="250" autofocus="" value="<?php echo $doku['name']?>">

			</div>

			<div class="form-group" <?php if (isset($error['noLink'])): ?> style="color: red;" <?php endif; ?>>

				<label>Link: <font color="red">*</font></label>

					<input class="form-control" required="" type="text" name="link" maxlength="250" autofocus="" value="<?php echo $doku['link']?>">

			</div>

			<div class="form-group" <?php if (isset($error['noLength'])): ?> style="color: red;" <?php endif; ?>>

				<label>Dauer: <font color="red">*</font></label>

					<input class="form-control" required="" type="text" name="dauer" maxlength="250" autofocus="" value="<?php echo $doku['dauer']?>">

			</div>

			<div class="form-group" <?php if (isset($error['noTags'])): ?> style="color: red;" <?php endif; ?>> 

				<label>Tags: <font color="red">*</font></label>

					<input class="form-control" required="" type="text" name="tags" maxlength="250" autofocus="" value="<?php echo $doku['tags']?>">

			</div>

			<div class="form-group">

				<label>Thema/Hinweise:</label>

					<input class="form-control" type="text" name="hint" maxlength="250" autofocus="" value="<?php echo $doku['hint']?>">

			</div>

			<div class="form-group">

				<label>Sprache: <font color="red">*</font></label>

					<select class="form-control" name="sprache">

						<option value="0">DE</option>

						<option value="1">EN</option>

						<option value="2">DE/EN</option>

						<option value="3">Andere</option>

					</select>

			</div>

			<div class="form-group">

				<label>Platform: <font color="red">*</font></label>

					<select class="form-control" name="platform">

						<option value="1">Youtube</option>

						<option value="2">Netflix</option>

						<option value="3">Andere</option>

					</select>

			</div>

			<div class="form-group mb-5">

				<input type="text" name="token" value="<?php echo $_SESSION['csrf_token'] ?>" class="invisible" style="display: none;">

				<input type="submit" class="btn btn-success" name="edit" value="Doku hinzufügen">

			</div>

		</form>

	</div>

	<?php include "templates/footer.php";?>

</body>

</html>